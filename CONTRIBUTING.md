You want to contribute to the project ? Great ! You'll find here how to achieve that :
## Adding new language
1. Open the administration panel (menu : tools | addons | clipLO)
2. Clic "Add new language" button and enter languages values.
3. Add all keyword (see "Set new keyword")
4. See "How to integrate changes". You'll need to upload "base_XX.odb" *and* "cliplo.odb" files (where XX is the new language)

## Add new cliparts
1. New clipart needs to be in "svg" format *and* under "free" licence (see "allowed licences")
2. you also need to have a "png" of size 90x90px (Linux users may want to use "SVGtoPNG.sh" script from the main repo)
3. Add new files in "YOUR_CONFIG_FOLDER/libreoffice/4/user/uno_packages/cache/uno_packages/EXTENSION_ID/clipLO.oxt/clipLO" in Gallery and Thumbs folders.
4. Add keywords for the cliparts (see "Set new keyword")
5. See "How to integrate changes". You'll need to upload both svg and png files, as well as licence and origin of the file.

## Set new keywords
This will scan the folders for cliparts without keywords and let you set them
1. Open the administration panel (menu : tools | addons | clipLO)
2. Choose the language
3. Clic on "update database" button
4. For each clipart with no keywords in the database, it will let you set them, as well as the clipart licenece and origin.
5. See "How to integrate changes". You'll need to upload "base_XX.odb" (where XX is your chosen language).

## How to integrate changes
There's 2 main way to ask for integrating your changes :
- The "git" one : your create a fork, upload your changes there and create a merge request
- The "direct" one : create a new issue, upload the changed files there, and don't forget to explain with as much details as possible what you have done.

In both cases, I'll get a notification and review your work ASAP.
Thanks !
