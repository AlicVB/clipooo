# clipLO
![alt text](https://framagit.org/AlicVB/clipooo/-/raw/main/screenshots/fenetre_principale.png "fenetre")

## Description

clipLO est une extension pour [Libreoffice](https://fr.libreoffice.org/) qui permet l'ajout simple et rapide d'image de type "clipart" sous licence libre.

Il contient une base propre de 1300 images vectorielle issues du projet [OpenClipArt](https://openclipart.org/), ainsi que quelques images personnelles.

Il permet aussi de rechercher des images directement sur le site [Arasaac](https://arasaac.org/pictograms/search).

## Installation

1. Télécharger et installer une version récente de [Libreoffice](https://fr.libreoffice.org/).
2. Télécharger l'extension [ici](https://framagit.org/AlicVB/clipooo/-/raw/main/extensions/clipLO.oxt?ref_type=heads).
3. Ouvrir le fichier ".oxt" téléchargé avec [Libreoffice](https://fr.libreoffice.org/).
Et voilà !

## Utilisation

L'extension ajoute une barre d'outils spécifique avec son icone:
<img src="https://framagit.org/AlicVB/clipooo/-/raw/main/clipLO.svg?ref_type=heads&inline=false"  width="64" height="64">
Ainsi qu'une entrée dans le menu outils - addons.

Pour les utilisateurs d'une interface "groupée", Elle est disponible dans le menu "extension".

Ces boutons vous permettrons d'ouvrir la fenêtre principale du module.

Tapez le texte recherché dans la barre du haut, validez... et si votre recherche aboutit, vous verrez apparaitre les vignettes des images correspondantes.

Double cliquez sur les images pour les insérer.

## Détails

1. La texte entré dans la barre du haut sera recherché à l'intérieur de chaque mot clé.
Ce qui permet de rechercher par "son" : Taper "oi" renverra "roi", "toit", "voiture", etc
2. En cochant la case "Arasaac", la recherche inclurera les images issues de [ce site web](https://arasaac.org/pictograms/search).
Par conséquent, il pourra y avoir un peu de latence, suivant la vitesse de votre connexion internet.
3. Les images sont *copiées* dans votre document.
Ce qui signifie qu'elles seront toujours présentes, même si vous désinstallez l'extension ou perdez votre connexion internet.

## Bugs

Si vous découvrez un bug ou que vous souhaitez l'ajout de nouvelle fonctionnalités, vous pouvez utiliser le menu "issues" de la barre du haut de cette page web.

## Auteur

Aldric Renaudin

## License

Ce programme est distribué sous la licence libre [GPL V3](https://www.gnu.org/licenses/gpl-3.0.en.html).

Ce qui signifie que vous pouvez l'utiliser gratuitement et même en modifier le code source si vous le souhaitez.
