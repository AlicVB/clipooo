#!/bin/bash
#
#***********************************************************************
# This file is part of ClipOOo,
# copyright (c) 2007--2011 Aldric Renaudin (AlicVB).
#
# ClipOOo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ClipOOo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ClipOOo.  If not, see <http://www.gnu.org/licenses/>.
#***********************************************************************


#
#This script scan all svg files in ./oxt/ClipOOo/Gallery/*
#if a file has no corresponding thumb or "big" png file
#the script create them
#The script cleanup png without corresponding svg too
#
#This script require 'inkscape' to be installed
#

DIR="$( cd "$( dirname "$0" )" && pwd )"

dos="$DIR/oxt/ClipOOo/Gallery/"
dospng="$DIR/png/"
dosthumb="$DIR/oxt/ClipOOo/Thumbs/"

for i in `ls $dos`;
do
	if [ -f $dos$i ]
	then
		true
	else
		dos2=$dos$i"/"
		dospng2=$dospng$i"/"
		dosthumb2=$dosthumb$i"/"
		echo "Directory : "$dos2
		`mkdir $dospng2`
		`mkdir $dosthumb2`
		for j in `ls $dos2`;
		do
		  if [ -f $dos2$j ]
		  then
			fic1=$dos2$j
			if [ ${fic1:(-4)} = ".svg" ]
			then
				echo "--------convert the file : "$j
				
				ficx=`basename $fic1 ".svg"`
				fic2=$dospng2$ficx".png"
				fic3=$dosthumb2$ficx".png"
				#creation of the thumb
				if [ -f $fic3 ]
				then
					echo "File already exists"
				else
					#we get height and width of the file
					l=`inkscape -W $fic1`
					h=`inkscape -H $fic1`
					l=${l/\.*}
					h=${h/\.*}
					if [ $l -gt $h ]
					then
						echo `inkscape --export-png=$fic3 --export-width=90 $fic1`
					else
						echo `inkscape --export-png=$fic3 --export-height=90 $fic1`
					fi
					echo "------------OK"
					echo ""
				fi
			fi
		   fi
		done
	fi
done
#now we scan thumbs to find png without svg
echo "----------Scanning the thumbs to find obsolete one"
for i in `ls $dosthumb`;
do
	if [ -f $dosthumb$i ]
	then
		true
	else
		dos2=$dosthumb$i"/"
		dossvg=$dos$i"/"
		echo "Directory : "$dos2
		for j in `ls $dos2`;
		do
		  if [ -f $dos2$j ]
		  then
			fic1=$dos2$j
			ficx=`basename $fic1 ".png"`
			fic1=$dossvg$ficx".svg"
			if [ -f "$fic1" ]
			then
				true
			else
				echo "  remove $dos2$j"
				`rm "$dos2$j"`
			fi
		   fi
		done
	fi
done
echo "....done"
